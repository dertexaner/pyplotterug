#!/usr/bin/env python
from setup.pyPlotter import *
# ---------------------------------------------------------------------------------
#gROOT.ProcessLineSync('.L tools/upgradeCustomFunctions.C+') #included in setup/bla..
gROOT.ProcessLineSync('.L tools/essentials.C+')
# ---------------------------------------------------------------------------------
print "runPlotter.py BEGIN"


# ---------------------------------------------------------------------------------
#   Analysis name, used to create the output directory & files
# ---------------------------------------------------------------------------------
#pset.analysis="UpgradeMatchingEfficiencyJune13"
pset.analysis="UpgradeMatchingEfficiencyJune20"
pset.fraction = 1.0
pset.pumode = "none"
pset.hpuName = ""
#pset.nloweightName = ""
pset.hcountName = ""
pset.lumi=1.


# ---------------------------------------------------------------------------------
#   Variable settings
# ---------------------------------------------------------------------------------
pset.setAliases=True # WARNING:: See alias definitions here: tools/Aliases.py
pset.aliases = []
pset.aliases.append( ('L1TrackDRminGenParticle','minDRVMaker(L1TrackEta,L1TrackPhi,GenParticleEta,GenParticlePhi)') )
pset.aliases.append( ('L1EGXtalClusterDRminGenParticle','minDRVMaker(L1EGXtalClusterNoCutsEta,L1EGXtalClusterNoCutsPhi,GenParticleEta,GenParticlePhi)') )
pset.aliases.append( ('GenParticleDRminL1Track','minDRVMaker(GenParticleEta,GenParticlePhi,L1TrackEta,L1TrackPhi)') )
pset.aliases.append( ('GenParticleDRminL1EGXtalCluster','minDRVMaker(GenParticleEta,GenParticlePhi,L1EGXtalClusterNoCutsEta,L1EGXtalClusterNoCutsPhi)') )
pset.aliases.append( ('L1TrackCount','Sum(L1TrackPt>0)') )
#
pset.aliases.append( ('EcalPt','combineVector(L1EGammaCollectionBXVWithCutsPt,L1EGammaCollectionBXVWithCutsPtEE)') )
pset.aliases.append( ('EcalEta','combineVector(L1EGammaCollectionBXVWithCutsEta,L1EGammaCollectionBXVWithCutsEtaEE)') )
pset.aliases.append( ('EcalPhi','combineVector(L1EGammaCollectionBXVWithCutsPhi,L1EGammaCollectionBXVWithCutsPhiEE)') )
pset.aliases.append( ('TrackPt','L1TrackPt') )
pset.aliases.append( ('TrackEta','L1TrackEta') )
pset.aliases.append( ('TrackPhi','L1TrackPhi') )
pset.aliases.append( ('TrackCharge','L1TrackCharge') )
pset.aliases.append( ('TrackZ0','L1TrackZ0') )
pset.aliases.append( ('TrackD0','L1TrackD0') )
pset.aliases.append( ('TrackTheta','etaToTheta(TrackEta)') )
pset.aliases.append( ('TrackPhiCorr','phiCorr(TrackPt,TrackEta,TrackPhi,TrackCharge,0.775)') )
pset.aliases.append( ('TrackPhiAbsDelta','absDeltaPhi(TrackPhi,TrackPhiCorr)') )
#
# ECAL - Trk matching in the barrel and endcap!
pset.aliases.append( ('EcalTrackDRMin','minDRVMaker(EcalEta,EcalPhi,TrackEta,TrackPhi)') )
#
pset.aliases.append( ('EcalTrackDPhiAtDR0p3MatchPt','dPhiAtDRMatchPtVMaker(EcalPt,EcalEta,EcalPhi,TrackPt,TrackEta,TrackPhi,TrackCharge,0.3)') )
pset.aliases.append( ('EcalTrackDPhiAtDR0p3MatchPtCorr','dPhiAtDRMatchPtVMaker(EcalPt,EcalEta,EcalPhi,TrackPt,TrackEta,TrackPhi,TrackCharge,0.3,0.88)') ) # original default 0.775
pset.aliases.append( ('EcalTrackDPhiAtDR0p3MatchPtCorr1','dPhiAtDRMatchPtVMaker(EcalPt,EcalEta,EcalPhi,TrackPt,TrackEta,TrackPhi,TrackCharge,0.3,0.5)') )
pset.aliases.append( ('EcalTrackDPhiAtDR0p3MatchPtCorr2','dPhiAtDRMatchPtVMaker(EcalPt,EcalEta,EcalPhi,TrackPt,TrackEta,TrackPhi,TrackCharge,0.3,0.6)') )
pset.aliases.append( ('EcalTrackDPhiAtDR0p3MatchPtCorr3','dPhiAtDRMatchPtVMaker(EcalPt,EcalEta,EcalPhi,TrackPt,TrackEta,TrackPhi,TrackCharge,0.3,0.7)') )
pset.aliases.append( ('EcalTrackDPhiAtDR0p3MatchPtCorr4','dPhiAtDRMatchPtVMaker(EcalPt,EcalEta,EcalPhi,TrackPt,TrackEta,TrackPhi,TrackCharge,0.3,0.75)') )
pset.aliases.append( ('EcalTrackDPhiAtDR0p3MatchPtCorr5','dPhiAtDRMatchPtVMaker(EcalPt,EcalEta,EcalPhi,TrackPt,TrackEta,TrackPhi,TrackCharge,0.3,0.8)') )
pset.aliases.append( ('EcalTrackDPhiAtDR0p3MatchPtCorr6','dPhiAtDRMatchPtVMaker(EcalPt,EcalEta,EcalPhi,TrackPt,TrackEta,TrackPhi,TrackCharge,0.3,0.83)') ) 
pset.aliases.append( ('EcalTrackDPhiAtDR0p3MatchPtCorr7','dPhiAtDRMatchPtVMaker(EcalPt,EcalEta,EcalPhi,TrackPt,TrackEta,TrackPhi,TrackCharge,0.3,0.85)') )
pset.aliases.append( ('EcalTrackDPhiAtDR0p3MatchPtCorr8','dPhiAtDRMatchPtVMaker(EcalPt,EcalEta,EcalPhi,TrackPt,TrackEta,TrackPhi,TrackCharge,0.3,0.88)') )
pset.aliases.append( ('EcalTrackDPhiAtDR0p3MatchPtCorr9','dPhiAtDRMatchPtVMaker(EcalPt,EcalEta,EcalPhi,TrackPt,TrackEta,TrackPhi,TrackCharge,0.3,0.89)') )
pset.aliases.append( ('EcalTrackDPhiAtDR0p3MatchPtCorr10','dPhiAtDRMatchPtVMaker(EcalPt,EcalEta,EcalPhi,TrackPt,TrackEta,TrackPhi,TrackCharge,0.3,0.90)') )
pset.aliases.append( ('EcalTrackDPhiAtDR0p3MatchPtCorr11','dPhiAtDRMatchPtVMaker(EcalPt,EcalEta,EcalPhi,TrackPt,TrackEta,TrackPhi,TrackCharge,0.3,0.91)') )
pset.aliases.append( ('EcalTrackDPhiAtDR0p3MatchPtCorr12','dPhiAtDRMatchPtVMaker(EcalPt,EcalEta,EcalPhi,TrackPt,TrackEta,TrackPhi,TrackCharge,0.3,0.92)') )
pset.aliases.append( ('EcalTrackDPhiAtDR0p3MatchPtCorr13','dPhiAtDRMatchPtVMaker(EcalPt,EcalEta,EcalPhi,TrackPt,TrackEta,TrackPhi,TrackCharge,0.3,0.95)') )
pset.aliases.append( ('EcalTrackDPhiAtDR0p3MatchPtCorr14','dPhiAtDRMatchPtVMaker(EcalPt,EcalEta,EcalPhi,TrackPt,TrackEta,TrackPhi,TrackCharge,0.3,1.0)') )
pset.aliases.append( ('EcalTrackDPhiAtDR0p3MatchPtCorr15','dPhiAtDRMatchPtVMaker(EcalPt,EcalEta,EcalPhi,TrackPt,TrackEta,TrackPhi,TrackCharge,0.3,1.1)') )
#
pset.aliases.append( ('EcalTrackDZAtDR0p3MatchPt',    'dZAtDRMatchPtVMaker(EcalPt,EcalEta,EcalPhi,TrackPt,TrackEta,TrackPhi,TrackZ0,0.3,0.0,0.0)') )
pset.aliases.append( ('EcalTrackDZAtDR0p3MatchPtCorr','dZAtDRMatchPtVMaker(EcalPt,EcalEta,EcalPhi,TrackPt,TrackEta,TrackPhi,TrackZ0,0.3,0.02)') )
pset.aliases.append( ('EcalTrackDZAtDR0p3MatchPtCorr1','dZAtDRMatchPtVMaker(EcalPt,EcalEta,EcalPhi,TrackPt,TrackEta,TrackPhi,TrackZ0,0.3,-0.05)') )
pset.aliases.append( ('EcalTrackDZAtDR0p3MatchPtCorr2','dZAtDRMatchPtVMaker(EcalPt,EcalEta,EcalPhi,TrackPt,TrackEta,TrackPhi,TrackZ0,0.3,-0.04)') )
pset.aliases.append( ('EcalTrackDZAtDR0p3MatchPtCorr3','dZAtDRMatchPtVMaker(EcalPt,EcalEta,EcalPhi,TrackPt,TrackEta,TrackPhi,TrackZ0,0.3,-0.03)') )
pset.aliases.append( ('EcalTrackDZAtDR0p3MatchPtCorr4','dZAtDRMatchPtVMaker(EcalPt,EcalEta,EcalPhi,TrackPt,TrackEta,TrackPhi,TrackZ0,0.3,-0.02)') )
pset.aliases.append( ('EcalTrackDZAtDR0p3MatchPtCorr5','dZAtDRMatchPtVMaker(EcalPt,EcalEta,EcalPhi,TrackPt,TrackEta,TrackPhi,TrackZ0,0.3,-0.01)') )
pset.aliases.append( ('EcalTrackDZAtDR0p3MatchPtCorr6','dZAtDRMatchPtVMaker(EcalPt,EcalEta,EcalPhi,TrackPt,TrackEta,TrackPhi,TrackZ0,0.3,0.0)') )
pset.aliases.append( ('EcalTrackDZAtDR0p3MatchPtCorr7','dZAtDRMatchPtVMaker(EcalPt,EcalEta,EcalPhi,TrackPt,TrackEta,TrackPhi,TrackZ0,0.3,0.01)') )
pset.aliases.append( ('EcalTrackDZAtDR0p3MatchPtCorr8','dZAtDRMatchPtVMaker(EcalPt,EcalEta,EcalPhi,TrackPt,TrackEta,TrackPhi,TrackZ0,0.3,0.02)') )
pset.aliases.append( ('EcalTrackDZAtDR0p3MatchPtCorr9','dZAtDRMatchPtVMaker(EcalPt,EcalEta,EcalPhi,TrackPt,TrackEta,TrackPhi,TrackZ0,0.3,0.03)') )
pset.aliases.append( ('EcalTrackDZAtDR0p3MatchPtCorr10','dZAtDRMatchPtVMaker(EcalPt,EcalEta,EcalPhi,TrackPt,TrackEta,TrackPhi,TrackZ0,0.3,0.04)') )
pset.aliases.append( ('EcalTrackDZAtDR0p3MatchPtCorr11','dZAtDRMatchPtVMaker(EcalPt,EcalEta,EcalPhi,TrackPt,TrackEta,TrackPhi,TrackZ0,0.3,0.05)') )

pset.aliases.append( ('EcalTrackDZAtDR0p3MatchPtCorr61','dZAtDRMatchPtVMaker(EcalPt,EcalEta,EcalPhi,TrackPt,TrackEta,TrackPhi,TrackZ0,0.3,0.0,0.6)') )
pset.aliases.append( ('EcalTrackDZAtDR0p3MatchPtCorr62','dZAtDRMatchPtVMaker(EcalPt,EcalEta,EcalPhi,TrackPt,TrackEta,TrackPhi,TrackZ0,0.3,0.0,0.7)') )
pset.aliases.append( ('EcalTrackDZAtDR0p3MatchPtCorr63','dZAtDRMatchPtVMaker(EcalPt,EcalEta,EcalPhi,TrackPt,TrackEta,TrackPhi,TrackZ0,0.3,0.0,0.8)') )
pset.aliases.append( ('EcalTrackDZAtDR0p3MatchPtCorr64','dZAtDRMatchPtVMaker(EcalPt,EcalEta,EcalPhi,TrackPt,TrackEta,TrackPhi,TrackZ0,0.3,0.0,0.9)') )
pset.aliases.append( ('EcalTrackDZAtDR0p3MatchPtCorr65','dZAtDRMatchPtVMaker(EcalPt,EcalEta,EcalPhi,TrackPt,TrackEta,TrackPhi,TrackZ0,0.3,0.0,1.0)') )
pset.aliases.append( ('EcalTrackDZAtDR0p3MatchPtCorr66','dZAtDRMatchPtVMaker(EcalPt,EcalEta,EcalPhi,TrackPt,TrackEta,TrackPhi,TrackZ0,0.3,0.0,1.1)') )
pset.aliases.append( ('EcalTrackDZAtDR0p3MatchPtCorr67','dZAtDRMatchPtVMaker(EcalPt,EcalEta,EcalPhi,TrackPt,TrackEta,TrackPhi,TrackZ0,0.3,0.0,1.2)') )
pset.aliases.append( ('EcalTrackDZAtDR0p3MatchPtCorr68','dZAtDRMatchPtVMaker(EcalPt,EcalEta,EcalPhi,TrackPt,TrackEta,TrackPhi,TrackZ0,0.3,0.0,1.3)') )
pset.aliases.append( ('EcalTrackDZAtDR0p3MatchPtCorr69','dZAtDRMatchPtVMaker(EcalPt,EcalEta,EcalPhi,TrackPt,TrackEta,TrackPhi,TrackZ0,0.3,0.0,1.4)') )

#
pset.aliases.append( ('EcalTrackDLAtDR0p3MatchPt',    'dLAtDRMatchPtVMaker(EcalPt,EcalEta,EcalPhi,TrackPt,TrackEta,TrackPhi,TrackZ0,0.3,0.0,0.0)') )
pset.aliases.append( ('EcalTrackDLAtDR0p3MatchPtCorr','dLAtDRMatchPtVMaker(EcalPt,EcalEta,EcalPhi,TrackPt,TrackEta,TrackPhi,TrackZ0,0.3,-0.02)') )
#--------------

pset.branchesToSave = [
 'L1EGXtalClusterNoCutsEta'
,'L1EGXtalClusterNoCutsPhi'
,'L1EGXtalClusterNoCutsPt'
,'L1TrackEta'
,'L1TrackPhi'
,'L1TrackPt'
,'L1TrackCount'

,'L1TrackBarrelLayerHits'
,'L1TrackCharge'
,'L1TrackEndcapDiskHits'
,'L1TrackSeed'
,'L1TrackStubN'

,'GenParticleEta'
,'GenParticlePhi'
,'GenParticlePt'
,'L1TrackDRminGenParticle'
,'L1EGXtalClusterDRminGenParticle'
,'GenParticleDRminL1Track'
,'GenParticleDRminL1EGXtalCluster'

,'EcalPt'
,'EcalEta'
,'EcalPhi'
,'TrackPt'
,'TrackEta'
,'TrackPhi'
,'TrackCharge'
,'TrackZ0'
,'TrackD0'
,'TrackTheta'
,'TrackPhiCorr'
,'TrackPhiAbsDelta'

,'EcalTrackDRMin'
,'EcalTrackDPhiAtDR0p3MatchPt'
,'EcalTrackDPhiAtDR0p3MatchPtCorr'
,'EcalTrackDZAtDR0p3MatchPt'
,'EcalTrackDZAtDR0p3MatchPtCorr'
,'EcalTrackDLAtDR0p3MatchPt'
,'EcalTrackDLAtDR0p3MatchPtCorr'

,'EcalTrackDPhiAtDR0p3MatchPtCorr1'
,'EcalTrackDPhiAtDR0p3MatchPtCorr2'
,'EcalTrackDPhiAtDR0p3MatchPtCorr3'
,'EcalTrackDPhiAtDR0p3MatchPtCorr4'
,'EcalTrackDPhiAtDR0p3MatchPtCorr5'
,'EcalTrackDPhiAtDR0p3MatchPtCorr6'
,'EcalTrackDPhiAtDR0p3MatchPtCorr7'
,'EcalTrackDPhiAtDR0p3MatchPtCorr8'
,'EcalTrackDPhiAtDR0p3MatchPtCorr9'
,'EcalTrackDPhiAtDR0p3MatchPtCorr10'
,'EcalTrackDPhiAtDR0p3MatchPtCorr11'
,'EcalTrackDPhiAtDR0p3MatchPtCorr12'
,'EcalTrackDPhiAtDR0p3MatchPtCorr13'

,'EcalTrackDZAtDR0p3MatchPtCorr1'
,'EcalTrackDZAtDR0p3MatchPtCorr2'
,'EcalTrackDZAtDR0p3MatchPtCorr3'
,'EcalTrackDZAtDR0p3MatchPtCorr4'
,'EcalTrackDZAtDR0p3MatchPtCorr5'
,'EcalTrackDZAtDR0p3MatchPtCorr6'
,'EcalTrackDZAtDR0p3MatchPtCorr7'
,'EcalTrackDZAtDR0p3MatchPtCorr8'
,'EcalTrackDZAtDR0p3MatchPtCorr9'
,'EcalTrackDZAtDR0p3MatchPtCorr10'
,'EcalTrackDZAtDR0p3MatchPtCorr11'

,'EcalTrackDZAtDR0p3MatchPtCorr61'
,'EcalTrackDZAtDR0p3MatchPtCorr62'
,'EcalTrackDZAtDR0p3MatchPtCorr63'
,'EcalTrackDZAtDR0p3MatchPtCorr64'
,'EcalTrackDZAtDR0p3MatchPtCorr65'
,'EcalTrackDZAtDR0p3MatchPtCorr66'
,'EcalTrackDZAtDR0p3MatchPtCorr67'
,'EcalTrackDZAtDR0p3MatchPtCorr68'
,'EcalTrackDZAtDR0p3MatchPtCorr69'
]

pset.variables=[#Parameters: VariableName, nBins, xLow, xHigh, custom xLabel, isDiscrete/isContinuous
# WARNING:: Discrete variables need to be defined with bins of width 1, centered on integer numbers; i.e. bin at "2" should have a low-edge at 1.5, and high-edge at 2.5
 ("L1TrackCount",6,-0.5,5.5,"L1TrackN",pset.isDiscrete)
,("L1EGammaCollectionBXVWithCutsEta[]",20,-3,3,"L1EGammaCollectionBXVWithCutsEta",pset.isContinuous)
,("L1EGammaCollectionBXVWithCutsPhi[]",5,-3.5,3.5,"L1EGammaCollectionBXVWithCutsPhi",pset.isContinuous)
,("L1EGammaCollectionBXVWithCutsPt[]",12,0,120,"L1EGammaCollectionBXVWithCutsPt",pset.isContinuous)
,("L1EGammaCollectionBXVWithCutsEtaEE[]",20,-3,3,"L1EGammaCollectionBXVWithCutsEtaEE",pset.isContinuous)
,("L1EGammaCollectionBXVWithCutsPhiEE[]",5,-3.5,3.5,"L1EGammaCollectionBXVWithCutsPhiEE",pset.isContinuous)
,("L1EGammaCollectionBXVWithCutsPtEE[]",12,0,120,"L1EGammaCollectionBXVWithCutsPtEE",pset.isContinuous)
,("L1EGXtalClusterNoCutsEta[]",20,-3,3,"L1EGXtalClusterNoCutsEta",pset.isContinuous)
,("L1EGXtalClusterNoCutsPhi[]",5,-3.5,3.5,"L1EGXtalClusterNoCutsPhi",pset.isContinuous)
,("L1EGXtalClusterNoCutsPt[]",12,0,120,"L1EGXtalClusterNoCutsPt",pset.isContinuous)
,("L1TrackEta[]",20,-3,3,"L1TrackEta",pset.isContinuous)
,("L1TrackPhi[]",35,-3.5,3.5,"L1TrackPhi",pset.isContinuous)
,("L1TrackPt[]",12,0,120,"L1TrackPt",pset.isContinuous)
,("L1TrackCharge[]",3,-1.5,1.5,"L1TrackCharge",pset.isDiscrete)
,("L1TrackSeed[]",15,-0.5,14.5,"L1TrackSeed",pset.isDiscrete)
,("GenParticleEta[]",20,-3,3,"GenParticleEta",pset.isContinuous)
,("GenParticlePhi[]",35,-3.5,3.5,"GenParticlePhi",pset.isContinuous)
,("GenParticlePt[]",12,0,120,"GenParticlePt",pset.isContinuous)
]


pset.globalselection="1"


# ---------------------------------------------------------------------------------
#   Local selection settings (these cuts act on the THn defined by pset.variables)
# ---------------------------------------------------------------------------------
pset.localselection=[ #Example line: "parameter1>cut1&&parameter2<cut2&&parameter3==cut3"
# WARNING:: Only use the following (in)equality relations: "<", ">", "=="    (== is for discrete variables only).
# WARNING:: Multiple local selection cuts cannot be applied on the same variable, i.e. MET>50 && MET<100 is dis-allowed! 
#           Instead, define and use an alias such as isMET50to100=(MET>50&&MET<100). See tools/Aliases.py for alias definitions.
# WARNING:: "1" is the no-local-selection case.
 "1"
]

# ---------------------------------------------------------------------------------
#   Input settings
# ---------------------------------------------------------------------------------
pset.inputs = [ ] #Parameters: Name, Xsec (pb), File, NormFudge/Weight, RelFlatUnc., TechnicalSelection, TTreeName
pset.inputs = [
# ("Ext",1.,"results_Extended_SingleEleNoPU_10000_June13.root",1,0,"1","rootTupleTree/tree")
#,("Nom",1.,"results_Nominal_SingleEleNoPU_10000_June13.root",1,0,"1","rootTupleTree/tree")
  ("Ext",1.,"results_Extended_MinBiasNoPU_250K.root",1,0,"1","rootTupleTree/tree")
]

pset.commonMCWeights=[ #Weights for PU, lepton SF, etc.
#"Weight*(1>0)"
#,"leptonSF3D(LightLeptonPt,LightLeptonEta,LightLeptonFlavor,1)"
#,"trigEffSF3D(LightLeptonPt,LightLeptonEta,LightLeptonFlavor)"
]
pset.bundleWeights=[ #Weights for nJets, etc.
#("Data","Weight")
#,("MisID","Weight")
]


# ---------------------------------------------------------------------------------
#   Plot settings
# ---------------------------------------------------------------------------------
pset.useCustomXlabels=True
pset.showSelection=True
pset.usePoissonErrorBarsOnData=False # see: https://twiki.cern.ch/twiki/bin/viewauth/CMS/PoissonErrorBars
pset.doRatio=False
pset.doZscore=False #works only when doRatio is enabled.
pset.usePublicationStyle=False
pset.overlayBundles=[ "Ext" ]; pset.bundleColors=["Ext==2","Nom==4" ]# applied only if all bundles defined above are accounted for!
#    INFO:: Root colors: (https://root.cern.ch/doc/master/pict1_TColor_002.png)
#              kWhite =0,   kBlack =1,   kGray=920,
#              kRed   =632, kGreen =416, kBlue=600, kYellow=400, kMagenta=616, kCyan=432,
#              kOrange=800, kSpring=820, kTeal=840, kAzure =860, kViolet =880, kPink=900.

# ---------------------------------------------------------------------------------
#   *** DO NOT TOUCH *** All default values
# ---------------------------------------------------------------------------------
pset.useUnityWeights=True

# ---------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------
runPlotter(pset,args)
# ---------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------


print "runPlotter.py END"
# ---------------------------------------------------------------------------------

