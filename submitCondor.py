#!/usr/bin/env python
import setup.argParser as args
import os, sys, time
# --------
# Make sure these make sense !!
#CMSSWdir  = "/cms/multilepton/cmssw/CMSSW_10_4_ROOT614/src/"
#ScramArch = "slc6_amd64_gcc700"
#if not os.path.exists(CMSSWdir) : print "ERROR:: Invalid CMSSW directory!\n ..exiting."; exit(1)
# --------


# get arguments:
runplotter = str(args.runplotter)
mode = str(args.mode)
copyall = bool(args.copyall)

if mode != "c" and mode != "p" and mode != "d" and mode != "cp" and mode != "pd" and mode != "cpd" :
    print "\nERROR::Invalid mode provided, valid modes for condor submission are c, p, d, cp, pd, or cpd"
    print "Exiting..\n"; exit(1)

# extract number of inputs:
inputn = -1
args.execute = False
module = __import__(runplotter.replace('.py', ''))
inputn = len(module.pset.inputs)
args.execute = True

# define file/folder variables:
cwd        = os.getcwd()
timestr    = time.strftime("%Y%m%d%H%M%S")
jobname    = runplotter.replace('.py','')+'_'+timestr
# create necessary folder structure here:
if not os.path.exists(cwd+"/condor/"): os.makedirs(cwd+"/condor/")
os.makedirs(cwd+"/condor/"+jobname)
# define output foldername
directoryName = runplotter.replace(".py","") # either get this from macro name, or via the pset.analysis setting!
if len(module.pset.analysis)>0 : directoryName = "runPlotter_"+module.pset.analysis
if not os.path.exists(directoryName): os.makedirs(directoryName) #doing this step in the individual jobs create problems!

# copy reference files:
os.system("cp "+cwd+"/"+runplotter+" "+cwd+"/condor/"+jobname+"/")
os.system("cp "+cwd+"/tools/Aliases.py "+cwd+"/condor/"+jobname+"/")

# delete existing *.root files when running in "create" mode (start with a clean slate):
if "c" in mode and any(".root" in s for s in os.listdir(directoryName+"/")) :
    os.system("rm -rf "+cwd+"/"+directoryName+"/*.root")
    os.system("rm -rf "+cwd+"/"+directoryName+"/LocalSelection*")

# delete existing *TH1*.root files when running in "project" mode
if "p" in mode and any("TH1" in s for s in os.listdir(directoryName+"/")) :
    os.system("rm -rf "+cwd+"/"+directoryName+"/*TH1*.root")
    os.system("rm -rf "+cwd+"/"+directoryName+"/LocalSelection*")

# archive the pdf file from the previous run *if present*
if any(".pdf" in s for s in os.listdir(directoryName+"/")) :
    os.system("mv "+cwd+"/"+directoryName+"/runPlotter*.pdf "+cwd+"/"+directoryName+"/archive"+timestr+".pdf")

# check if TH1s exist if running in "draw" mode, and if not, try to create it *if* other *.root files exist!:
if "d" in mode and "p" not in mode and not os.path.exists(directoryName+"/TH1.root") :
    print "\n"+directoryName+"/TH1.root does *not* exist, attempting to hadd it!"
    os.system("hadd "+directoryName+"/TH1.root "+directoryName+"/Input_*_TH1.root")



# Create condor submission files:
# --------
if "d" in mode or args.onejob :
    # create job shell file
    jobshellfile   = open(cwd+'/condor/'+jobname+'/condor_'+jobname+'.sh', 'w')
    print >> jobshellfile, '#!/bin/bash'
#    print >> jobshellfile, 'export SCRAM_ARCH='+ScramArch
    print >> jobshellfile, 'export VO_CMS_SW_DIR=/cms/base/cmssoft'
    print >> jobshellfile, 'export COIN_FULL_INDIRECT_RENDERING=1'
    print >> jobshellfile, 'echo $VO_CMS_SW_DIR'
    print >> jobshellfile, 'source $VO_CMS_SW_DIR/cmsset_default.sh'
#    print >> jobshellfile, 'cd '+CMSSWdir
#    print >> jobshellfile, 'eval `scramv1 runtime -sh`'
    print >> jobshellfile, 'source /cvmfs/sft.cern.ch/lcg/views/LCG_94/x86_64-slc6-gcc7-opt/setup.sh'
    print >> jobshellfile, 'cd '+cwd
    print >> jobshellfile, 'condordir=`echo $_CONDOR_SCRATCH_DIR`'
    print >> jobshellfile, 'condordirparsed=${condordir:1}'
    print >> jobshellfile, 'cp '+runplotter+' $condordir/'+runplotter
    print >> jobshellfile, 'mkdir $condordir/'+directoryName+'/'
    if "pd" in mode and "c" not in mode : print >> jobshellfile, 'cp '+cwd+'/'+directoryName+'/*Tree.root $condordir/'+directoryName+'/'
    print >> jobshellfile, 'cp '+cwd+'/'+directoryName+'/*TH1.root $condordir/'+directoryName+'/'
    print >> jobshellfile, 'mkdir $condordir/tools/'
    print >> jobshellfile, 'cp    tools/*.py  $condordir/tools/'
    print >> jobshellfile, 'cp    tools/*.C  $condordir/tools/'
    print >> jobshellfile, 'cp    tools/*.cc  $condordir/tools/'
    print >> jobshellfile, 'cp    tools/*.dat  $condordir/tools/'
    print >> jobshellfile, 'cp    tools/*.h  $condordir/tools/'
    print >> jobshellfile, 'cp    tools/*.cpp  $condordir/tools/'
    print >> jobshellfile, 'cp    tools/*.csv  $condordir/tools/'
    print >> jobshellfile, 'cp -r setup $condordir/'
    print >> jobshellfile, 'cd $condordir'
    print >> jobshellfile, 'time python '+runplotter+' +n $condordirparsed +a '+directoryName.replace("runPlotter_","")+' +m '+str(mode)
    #print >> jobshellfile, 'time python '+runplotter+' +n $condordirparsed +a '+directoryName.replace("runPlotter_","")+' +m '+str(mode)+' &> $_CONDOR_SCRATCH_DIR/job_$1.out'
    #print >> jobshellfile, 'cp $condordir/job_$1.out'+' '+cwd+'/condor/job_$1.out'
    print >> jobshellfile, 'cp $condordir/'+directoryName+'/*TH1.root '+cwd+'/'+directoryName+'/'
    print >> jobshellfile, 'cp $condordir/'+directoryName+'/*.pdf '+cwd+'/'+directoryName+'/'
    print >> jobshellfile, 'cp -r $condordir/'+directoryName+'/LocalSelection* '+cwd+'/'+directoryName+'/'
    if copyall : print >> jobshellfile, 'cp $condordir/'+directoryName+'/*Tree.root '+cwd+'/'+directoryName+'/'
    jobshellfile.close()
    #
    # create job control file
    jobcontrolfile = open(cwd+'/condor/'+jobname+'/condor_'+jobname+'.jcl', 'w')
    print >> jobcontrolfile, 'universe = vanilla'
    print >> jobcontrolfile, 'initialdir = '+cwd+'/condor/'+jobname+'/'
    print >> jobcontrolfile, 'error = '+cwd+'/condor/'+jobname+'/condor_'+jobname+'.error'
    print >> jobcontrolfile, 'log = '+cwd+'/condor/'+jobname+'/condor_'+jobname+'.log'
    print >> jobcontrolfile, 'output = '+cwd+'/condor/'+jobname+'/condor_'+jobname+'.out'
    print >> jobcontrolfile, 'executable = '+cwd+'/condor/'+jobname+'/condor_'+jobname+'.sh'
    print >> jobcontrolfile, 'arguments = $(Process)'
    print >> jobcontrolfile, 'Notification=never'
    print >> jobcontrolfile, 'rank = JavaMFlops'
    print >> jobcontrolfile, 'priority = 15'
    print >> jobcontrolfile, 'queue 1'
    jobcontrolfile.close()
    #
    # submit jobs
    os.system("source /condor/HTCondor/current/condor.sh")
    os.system("chmod 755 "+cwd+"/condor/"+jobname+"/condor_"+jobname+".sh")
    os.system("condor_submit "+cwd+"/condor/"+jobname+"/condor_"+jobname+".jcl")
# --------
else :
    for inputindex in xrange(0,inputn):
        # create job shell file
        jobshellfile   = open(cwd+'/condor/'+jobname+'/condor_'+jobname+'_'+str(inputindex)+'.sh', 'w')
        print >> jobshellfile, '#!/bin/bash'
#        print >> jobshellfile, 'export SCRAM_ARCH='+ScramArch
        print >> jobshellfile, 'export VO_CMS_SW_DIR=/cms/base/cmssoft'
        print >> jobshellfile, 'export COIN_FULL_INDIRECT_RENDERING=1'
        print >> jobshellfile, 'echo $VO_CMS_SW_DIR'
        print >> jobshellfile, 'source $VO_CMS_SW_DIR/cmsset_default.sh'
#        print >> jobshellfile, 'cd '+CMSSWdir
#        print >> jobshellfile, 'eval `scramv1 runtime -sh`'
        print >> jobshellfile, 'source /cvmfs/sft.cern.ch/lcg/views/LCG_94/x86_64-slc6-gcc7-opt/setup.sh'
        print >> jobshellfile, 'cd '+cwd
        print >> jobshellfile, 'condordir=`echo $_CONDOR_SCRATCH_DIR`'
        print >> jobshellfile, 'condordirparsed=${condordir:1}'
        print >> jobshellfile, 'cp '+runplotter+' $condordir/'+runplotter
        print >> jobshellfile, 'mkdir $condordir/'+directoryName+'/'
        if "p" in mode and "c" not in mode : print >> jobshellfile, 'cp '+cwd+'/'+directoryName+'/*Tree.root $condordir/'+directoryName+'/'
        print >> jobshellfile, 'mkdir $condordir/tools/'
        print >> jobshellfile, 'cp    tools/*.py  $condordir/tools/'
        print >> jobshellfile, 'cp    tools/*.C  $condordir/tools/'
        print >> jobshellfile, 'cp    tools/*.cc  $condordir/tools/'
        print >> jobshellfile, 'cp    tools/*.dat  $condordir/tools/'
        print >> jobshellfile, 'cp    tools/*.h  $condordir/tools/'
        print >> jobshellfile, 'cp    tools/*.cpp  $condordir/tools/'
        print >> jobshellfile, 'cp    tools/*.csv  $condordir/tools/'
        print >> jobshellfile, 'cp -r setup $condordir/'
        print >> jobshellfile, 'cd $condordir'
        print >> jobshellfile, 'time python '+runplotter+' +n $condordirparsed +ix '+str(inputindex)+' +a '+directoryName.replace("runPlotter_","")+' +m '+str(mode)
        #print >> jobshellfile, 'time python '+runplotter+' +n $condordirparsed +ix '+str(inputindex)+' +a '+directoryName.replace("runPlotter_","")+' +m '+str(mode)+' &> $condordir/job_'+jobname+'_'+str(inputindex)+'.out'
        #print >> jobshellfile, 'cp $condordir/job_'+jobname+'_'+str(inputindex)+'.out'+' '+cwd+'/condor/'+jobname+'/job_'+jobname+'_'+str(inputindex)+'.out'
        print >> jobshellfile, 'cp $condordir/'+directoryName+'/*TH1.root '+cwd+'/'+directoryName+'/'
        if copyall : print >> jobshellfile, 'cp $condordir/'+directoryName+'/*Tree.root '+cwd+'/'+directoryName+'/'
    jobshellfile.close()
    #            
    # create job control file
    for inputindex in xrange(0,inputn):
        jobcontrolfile = open(cwd+'/condor/'+jobname+'/condor_'+jobname+'_'+str(inputindex)+'.jcl', 'w')
        print >> jobcontrolfile, 'universe = vanilla'
        print >> jobcontrolfile, 'initialdir = '+cwd+'/condor/'+jobname+'/'
        print >> jobcontrolfile, '\n'
        #
        #for istep in xrange(1,args.maxsteps+1):
        for istep in xrange(0,1):
            print >> jobcontrolfile, 'error = '+cwd+'/condor/'+jobname+'/condor_'+jobname+'_'+str(inputindex)+'.error'
            print >> jobcontrolfile, 'log = '+cwd+'/condor/'+jobname+'/condor_'+jobname+'_'+str(inputindex)+'.log'
            print >> jobcontrolfile, 'output = '+cwd+'/condor/'+jobname+'/condor_'+jobname+'_'+str(inputindex)+'.out'
            print >> jobcontrolfile, 'executable = '+cwd+'/condor/'+jobname+'/condor_'+jobname+'_'+str(inputindex)+'.sh'
            #print >> jobcontrolfile, 'arguments = $(Process)'
            #print >> jobcontrolfile, 'arguments = '+str(istep)
            print >> jobcontrolfile, 'Notification=never'
            print >> jobcontrolfile, 'rank = JavaMFlops'
            print >> jobcontrolfile, 'priority = 15'
            print >> jobcontrolfile, 'queue 1'
            print >> jobcontrolfile, '\n'
        jobcontrolfile.close()
    #
    # submit jobs
    os.system("source /condor/HTCondor/current/condor.sh")
    for inputindex in xrange(0,inputn):
        os.system("chmod 755 "+cwd+"/condor/"+jobname+"/condor_"+jobname+"_"+str(inputindex)+".sh")
        os.system("condor_submit "+cwd+"/condor/"+jobname+"/condor_"+jobname+"_"+str(inputindex)+".jcl")
# --------
# --------
exit(0)
