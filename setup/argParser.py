#!/usr/bin/env python
import argparse, time, os
# --------------
def str2bool(v,label):
    v=str(v).replace(" ","").lower()
    if   v == "true"   : return True
    elif v == "false"  : return False
    else               : 
        print "Invalid input argument provided: ",label
        print "                                   boolean value True/False expected!"
        print "Exiting.."
        os._exit(0)
# --------------

parser = argparse.ArgumentParser(add_help=True,prefix_chars='+/')
#parser.add_argument( "+m" , "++mode",         dest="mode",         default="",  help="Set running mode for pyplotter: c-create/p-project/d-draw.", required=True, type=mode)
parser.add_argument( "+m" , "++mode",         dest="mode",         default="",  help="Set running mode for pyplotter: c-create/p-project/d-draw.", required=True )
parser.add_argument( "+a" , "++analysisname", dest="analysisname", default="",  help="Analysis name.", required=False )
parser.add_argument( "+s" , "++stamp",        dest="stamp",        action='store_true', default=False,  help="Create stamp for output directory.", required=False )
parser.add_argument( "+fs", "++forcestamp",   dest="forcestamp",   help="Assign stamp for output directory.", required=False )
parser.add_argument( "+ix", "++inputindex",   dest="inputindex",   default=-1, help="Input index for parallel processing.", required=False, type=int )
##parser.add_argument( "+t",  "++step",         dest="step",         default=0, help="Step (k/maxsteps) for parallel processing.", required=False, type=int )
##parser.add_argument( "+mt", "++maxsteps",     dest="maxsteps",     default=1, help="Step (k/maxsteps) for parallel processing.", required=False, type=int )
#### Only used in condor submissions :
parser.add_argument( "+r",  "++runplotter",   dest="runplotter",   default="", help="runPlotter macro!", required=False )
parser.add_argument( "+i",  "++inputn",       dest="inputn",       help="number of inputs", required=False, type=int )
#parser.add_argument( "+e",  "++execute",      dest="execute",      default=True,  help="Execute pyPlotter.", required=False, type=str2bool )
#parser.add_argument( "+o",  "++onejob",       dest="onejob",       default=False, help="Force-submit a single condor job!", required=False, type=str2bool )
parser.add_argument( "+e",  "++execute",      dest="execute",      default="True", help="Execute pyPlotter.", required=False )
parser.add_argument( "+o",  "++onejob",       dest="onejob",       default="False", help="Force-submit a single condor job!", required=False )
parser.add_argument( "+n",  "++nodedir",      dest="nodedir",      default="", help="Condor node output directory.", required=False )
parser.add_argument( "+c",  "++copyall",      dest="copyall",      default="True", help="Copy both TTree (could be large in size) and TH1 root files.", required=False )
parser.add_argument( "+d",  "++inputdir",     dest="inputdir",     default="", help="Input directory to overwrite the trees provided in \"pset.inputs\". Use as +d=/path/to/folder.", required=False )
parser.add_argument( "+fim", "++forceinputsmatch",   dest="forceinputsmatch",  default="True", help="Force configuration (global/technical selection, cross-sections etc.) of the input directory to match those in current runplotter macro.", required=False )
parser.print_help()
print
####
args = parser.parse_args()
####
analysisname     = str(args.analysisname)
mode             = args.mode
runplotter       = str(args.runplotter)
inputn           = args.inputn
inputindex       = args.inputindex
#step            = args.step
#maxsteps        = args.maxsteps
execute          = str2bool(args.execute,"+e/++execute")
onejob           = str2bool(args.onejob,"+o/++onejob")
copyall          = str2bool(args.copyall,"+c/++copyall")
inputdir         = str(args.inputdir)
forceinputsmatch = str2bool(args.forceinputsmatch,"+fim/forceinputsmatch")
stamp            = ""
nodedir          = ""
####
if args.stamp        : stamp=str(time.strftime("%Y%m%d%H%M%S"))
if args.forcestamp   : stamp=str(args.forcestamp)
if len(args.nodedir)>0 : nodedir = args.nodedir[1:]
#if args.inputindex   : inputindex=int(args.inputindex)
#if args.runplotter   : runplotter = str(args.runplotter)
#if args.inputn       : inputn = int(args.inputn)

