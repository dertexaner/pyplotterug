#!/usr/bin/env python
from ROOT import *

def CreateCanvas2D( canName ) :
    #can=TCanvas(canName,canName,0,0,650,650)
    can=TCanvas(canName,canName,0,0,750,650)
    can.cd()
    can.SetLogy(1)
    gStyle.SetOptStat(0)
    can.SetLeftMargin(0.13)
    can.SetRightMargin(0.12)
    can.SetTopMargin(0.07)
    can.SetBottomMargin(0.13)
    can.SetTickx(1)
    can.SetTicky(1)
    return can

def CreateCanvas1D( canName, canvasXsize=650, canvasYsize=650 ) :
    can=TCanvas(canName,canName,0,0,canvasXsize,canvasYsize)
    can.cd()
    can.SetLogy(1)
    gStyle.SetOptStat(0)
    can.SetLeftMargin(0.13)
    can.SetRightMargin(0.12)
    can.SetTopMargin(0.07)
    can.SetBottomMargin(0.13)
    can.SetTickx(1)
    can.SetTicky(1)
    return can

def CreatePad( padName, padYlow, padYhigh ) :
    pad = TPad(padName,padName, 0, padYlow, 1, padYhigh);
    pad.SetLogy(1)
    gStyle.SetOptStat(0)
    pad.SetLeftMargin(0.13)
    pad.SetRightMargin(0.12)
    pad.SetTopMargin(0)
    pad.SetBottomMargin(0)
    pad.SetTickx(1)
    pad.SetTicky(1)
    return pad

def SetgStyle():
    gStyle.SetOptStat(111110)
    gStyle.SetStatFontSize(0.04)
    gStyle.SetStatY(0.915)
    gStyle.SetStatX(0.93)
    gStyle.SetStatW(0.23)
    gStyle.SetStatH(0.25)

def SetRatioHistoStyle( histo, pset="" ) :
    histo.GetYaxis().SetRangeUser(0,2)
    histo.GetYaxis().SetTitleSize(0.145)
    histo.GetYaxis().SetTitleOffset(0.395)
    histo.GetYaxis().SetLabelOffset(0.013)
    histo.GetYaxis().SetLabelSize(0.115)
    histo.GetYaxis().SetNdivisions(505)
    histo.GetYaxis().SetTickLength(0.05)
    #
    histo.GetXaxis().SetLabelSize(0.1)
    histo.GetXaxis().SetTickLength(0.075)
    histo.GetXaxis().SetTitleSize(0.15)
    histo.GetXaxis().SetTitleOffset(1.12)
    histo.GetXaxis().SetLabelOffset(0.04)
    histo.GetXaxis().SetLabelSize(0.115)
    if pset : histo.GetYaxis().SetTitle(str(pset.ratioLabel))

def SetHistoStyle( histo, width=0, xlabel="") :
    histo.SetLineColor(2)
    histo.SetMarkerColor(1)
    histo.GetXaxis().SetTitleFont(42);
    histo.GetYaxis().SetTitleFont(42);
    histo.GetXaxis().SetTitleSize(0.05);
    histo.GetYaxis().SetTitleSize(0.05);
    histo.GetXaxis().SetTitleFont(42)
    histo.GetYaxis().SetTitleFont(42)
    histo.GetXaxis().SetLabelFont(42)
    histo.GetYaxis().SetLabelFont(42)
    histo.GetXaxis().SetTitleSize(0.055)
    histo.GetYaxis().SetTitleSize(0.055)
    histo.GetXaxis().SetTitleOffset(1.1)
    histo.GetYaxis().SetTitleOffset(1.1)
    histo.GetXaxis().SetLabelOffset(0.011)
    histo.GetYaxis().SetLabelOffset(0.011)
    histo.GetXaxis().SetLabelSize(0.045)
    histo.GetYaxis().SetLabelSize(0.045)
    histo.SetMarkerStyle(9)
    histo.SetMarkerColor(1)
    histo.SetFillColor(2)
    #histo.SetFillStyle(0)
    histo.SetFillStyle(1001)
    histo.SetTitle("")
    # if "(GeV)" in xlabel : 
       # if width.is_integer() : yaxisLabel = "Events / "+str(int(width))+" GeV"
       # else : yaxisLabel = "Events / "+str(width)+" GeV"
    # else : 
       # yaxisLabel = "Events"
    yaxisLabel = "Count"
    histo.GetYaxis().SetTitle(yaxisLabel)
    histo.GetYaxis().CenterTitle()
    histo.GetXaxis().CenterTitle()
    maxbin=histo.GetMaximumBin()
    maxheight=histo.GetBinContent(maxbin)
    histo.GetYaxis().SetRangeUser(0.2,maxheight*25)

def SetHistoStyle2D( histo ) :
    histo.SetLineColor(2)
    histo.SetMarkerColor(1)
    histo.GetXaxis().SetTitleFont(42);
    histo.GetYaxis().SetTitleFont(42);
    histo.GetXaxis().SetTitleSize(0.05);
    histo.GetYaxis().SetTitleSize(0.05);
    histo.GetXaxis().SetTitleFont(42)
    histo.GetYaxis().SetTitleFont(42)
    histo.GetXaxis().SetLabelFont(42)
    histo.GetYaxis().SetLabelFont(42)
    histo.GetXaxis().SetTitleSize(0.055)
    histo.GetYaxis().SetTitleSize(0.055)
    histo.GetXaxis().SetTitleOffset(1.1)
    histo.GetYaxis().SetTitleOffset(1.1)
    histo.GetXaxis().SetLabelOffset(0.011)
    histo.GetYaxis().SetLabelOffset(0.011)
    histo.GetXaxis().SetLabelSize(0.045)
    histo.GetYaxis().SetLabelSize(0.045)
    histo.SetMarkerStyle(9)
    histo.SetMarkerColor(1)
    histo.SetFillColor(2)
    histo.SetFillStyle(1001)
    histo.SetTitle("")
    histo.GetYaxis().CenterTitle()
    histo.GetXaxis().CenterTitle()
    histo.GetYaxis().SetRange(0,histo.GetNbinsY()+1)
    histo.GetXaxis().SetRange(0,histo.GetNbinsX()+1)
    

def SetLegendStyle( legend ) :
    legend.SetFillColor(0)
    legend.SetLineColor(0)
    legend.SetFillColor(0)
    legend.SetFillStyle(0)
    #legend.SetTextSize(0.045)
    legend.SetTextSize(0.0485)
    legend.SetTextFont(42)
    legend.SetTextAlign(12)
    legend.SetBorderSize(0)
