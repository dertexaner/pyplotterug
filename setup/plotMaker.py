#!/usr/bin/env python
from ROOT import *
from array import *
import setup.plotSettings as plotSettings
import tools.Stats as Stats
gROOT.SetBatch(True)
# ---------------------------------------------------------------------------------

def getKey4(item): return item[4]

# ---------------------------------------------------------------------------------

def effectiveWeight(mean,error):
    fracerror       = error/mean
    if fracerror>=1.0 : return mean
    effectivecount  = (1.0/pow(fracerror,2))
    effectiveweight = mean/effectivecount
    return effectiveweight

# ---------------------------------------------------------------------------------

def getAverageWeight(histo):
    averageweight=0
    nonzerobincount=0
    for ibin in xrange(1,histo.GetNbinsX()+1):
        if histo.GetBinContent(ibin) >0.01 :
            print "effectiveWeight(histo.GetBinContent(ibin),histo.GetBinError(ibin)",effectiveWeight(histo.GetBinContent(ibin),histo.GetBinError(ibin))," ",histo.GetBinContent(ibin)," ",histo.GetBinError(ibin)
            averageweight   = averageweight + effectiveWeight(histo.GetBinContent(ibin),histo.GetBinError(ibin))
            nonzerobincount = nonzerobincount + 1
    if nonzerobincount > 0 : return  averageweight/nonzerobincount
    else : return 0.01

def getPosIntegral(histo):
    posintegral=0;
    for ibin in xrange(1,histo.GetNbinsX()+1):
        if histo.GetBinContent(ibin) >0 : posintegral = posintegral + histo.GetBinContent(ibin)
    return posintegral 
            
# ---------------------------------------------------------------------------------


def smooth(histo):
    smoothhisto = histo.Clone("smoo")
    smoothhisto.Reset("MICES")
    smoothhisto.SetDirectory(0)
    temphisto=histo.Clone("temp")
    temphisto.SetDirectory(0)
    #
    firstbin        = (int)(1)
    secondbin       = (int)(2)
    secondtolastbin = (int)(histo.GetNbinsX()-1) 
    lastbin         = (int)(histo.GetNbinsX())
    #
    # Get positive integral and average per event weight of the original histo
    originalpositiveintegral=getPosIntegral(histo)
    effWeight=getAverageWeight(histo)
    print ">>>>>>> effWeight: ",effWeight
    print ">>>>>>> originalpositiveintegral: ",originalpositiveintegral
    #----
    # first fill in empty bins with average weight
    for ibin in xrange(1,temphisto.GetNbinsX()+1):
        print "original yield in bin(",ibin,"): ",histo.GetBinContent(ibin)," +/- ",histo.GetBinError(ibin)
        if temphisto.GetBinContent(ibin) < 0.01 :
            temphisto.SetBinContent(ibin,effWeight) # to propagate it down to smoothing part
            temphisto.SetBinError(ibin,effWeight) # to propagate it down to smoothing part
    #----
    if temphisto.GetNbinsX() > 2 :  # perform smoothing is histo has more than 2 bins!        
        for ibin in xrange(1, temphisto.GetNbinsX()+1):
            #print temphisto.GetBinLowEdge(ibin)," - ",temphisto.GetBinLowEdge(ibin)+temphisto.GetBinWidth(ibin)," : ",temphisto.GetBinContent(ibin)
            #print "smooth before ::: temphisto.GetBinContent(ibin): ",temphisto.GetBinContent(ibin)
            lowbin=0
            centerbin=0
            highbin=0
            smoothbin=0
            if ibin>=secondbin and ibin<=secondtolastbin  : # do not perform smoothing on the "edge" bins, and if histo has only 2 or 1 bins
                #print "smoothing in action"
                lowbin = max(temphisto.GetBinContent(ibin-1),0)
                centerbin = max(temphisto.GetBinContent(ibin),0)
                highbin = max(temphisto.GetBinContent(ibin+1),0)
                smoothbin=(lowbin+centerbin+highbin)/3.
                smoothdelta = abs(smoothbin-max(histo.GetBinContent(ibin),0))
                originalstaterror=histo.GetBinError(ibin)
                newstaterror = pow( pow(smoothdelta,2) + pow(originalstaterror,2) , 0.5 )
                smoothhisto.SetBinError(ibin,newstaterror)
                smoothhisto.SetBinContent(ibin,smoothbin)
            else : 
                smoothhisto.SetBinError(ibin,temphisto.GetBinError(ibin))
                smoothhisto.SetBinContent(ibin,temphisto.GetBinContent(ibin))
        #----
        smoothhisto.Scale(originalpositiveintegral/smoothhisto.Integral())
        return smoothhisto
    #----
    else : 
        return temphisto

# ---------------------------------------------------------------------------------

def plotMaker(pset) :
    import os
    print "\nBEGIN:: plotMaker\n"
    #
    rootFileName              = "runPlotter_"+pset.analysis
    globalselection           = pset.globalselection
    localselection            = pset.localselection
    #setAliases                = pset.setAliases
    variables                 = pset.variables
    lumi                      = pset.lumi
    inputs                    = pset.inputs
    useCustomXlabels          = pset.useCustomXlabels
    showSelection             = pset.showSelection
    usePoissonErrorBarsOnData = pset.usePoissonErrorBarsOnData
    doRatio                   = pset.doRatio
    doZscore                  = pset.doZscore
    useUnityWeights           = pset.useUnityWeights
    usePublicationStyle       = pset.usePublicationStyle
    bundleWeights             = pset.bundleWeights
    bundleColors              = pset.bundleColors
    overlayBundles            = pset.overlayBundles
    bundles                   = pset.bundles
    plotStatSystUnc           = pset.plotStatSystUnc
    plotUncUpperPanel         = pset.plotUncUpperPanel
    useOverlayForRatio        = pset.useOverlayForRatio
    ratioLabel                = pset.ratioLabel
    #
    # Extract bundles, and relative flat uncertainties:
    relflatuncs = []
    for iinput,(bundle,xsec,mfilename,normfudge,relflatunc,technicalselection,treename,isData) in enumerate(inputs):
        for ilocal in range(len(localselection)):
            rfu="h_"+str(ilocal)+"_localsel_"+str(iinput)+"_"+bundle+"_==="+str(relflatunc)
            relflatuncs.append(rfu)
    relflatuncsdic = dict(s.split('===') for s in relflatuncs)
    #
    print "\nBundles: "
    for bundle in bundles:
        print "  ",bundle
    print
    print "\nRelative Flat Uncertainties: "
    print relflatuncsdic


    # Check if all defined bundles have been accounted for in bundleColors:
    useBundleColors=True
    bundlecolorsdic = dict(s.split('==') for s in bundleColors)
    for bundle in bundles :
        if bundle not in bundlecolorsdic : useBundleColors=False

    # Read input file from setup/treeSkimmer.py:
    if not os.path.isfile(rootFileName+"/TH1.root") : print "ERROR:: input file not found!  inputFile=",rootFileName+"/TH1.root\n ..exiting!"; exit(0)
    inputR=TFile(rootFileName+"/TH1.root","READ")
    #print "\ninputR.ls():"
    #inputR.ls()
    #print 

    # Extract histograms in inputR:
    hitems = [k.GetName() for k in inputR.GetListOfKeys()]
    print "\nList inputR.GetListOfKeys():"
    for hitem in hitems:
        print "hitem: ", hitem
    print

    # Sum over all "bundles", for MC and for Data, also add flat uncertainties here:
    histoMCArray=[]
    histoDataArray=[]
    histoMisidentifiedSTATArray=[]
    # 
    #-------------
    # Extract reference histograms
    histoMCRefArray=[]
    for ilocal in range(len(localselection)):
        histoMCRefArrayPerLocalSel=[]
        if ilocal != pset.templateReferenceLS : continue
        for i,(var,nbin,binlow,binhigh,xlabel,isDiscrete) in enumerate(variables):
            histo=TH1F("","",nbin,binlow,binhigh)
            histo.Sumw2(kTRUE)
            histoSystUnc=TH1F("","",nbin,binlow,binhigh)
            #
            for bundle in bundles:
                if bundle != pset.templateBundle : continue
                histo.Reset("MICES")
                histoSystUnc.Reset("MICES")
                # Loop over all contributions for a given local selection, variable, and bundle
                for prebundleName in [hitemvar for hitemvar in hitems if hitemvar.endswith(bundle+"_"+var) and hitemvar.startswith("h_"+str(ilocal)+"_localsel_") ]:
                    h_=inputR.Get(prebundleName)
                    histo.Add(h_)
                    # Add flat uncertainty
                    flatuncertainty=relflatuncsdic[prebundleName[:-len(var)]]
                    print "LocalSelection: ",localselection[ilocal]," ",prebundleName,"  key: ",prebundleName[:-len(var)],"  flat uncertainty: ",flatuncertainty
                    hUnc_=h_.Clone("hUnc_")
                    hUnc_.Scale(float(flatuncertainty))
                    histoSystUnc.Add(hUnc_)
                # ----
                histo=smooth(histo)
                histointegral=0.0
                for ibin in xrange(1,histo.GetNbinsX()+2) : histointegral=histointegral+float(max(histo.GetBinContent(ibin),0))
                histoMCRefArrayPerLocalSel.append((histo.Clone("histo_"+var+bundle),histoSystUnc.Clone("histoSystUncertainty_"+var+bundle),bundle,var,histointegral))
        #
        histoMCRefArray.append(histoMCRefArrayPerLocalSel)
    #-------------
    #
    print
    print "histoMCRefArray: ",histoMCRefArray
    print
    for ilocal in range(len(localselection)):
        histoMCArrayPerLocalSel=[]
        histoDataArrayPerLocalSel=[]
        histoMisidentifiedSTATArrayPerLocalSel=[]
        for i,(var,nbin,binlow,binhigh,xlabel,isDiscrete) in enumerate(variables):
            histo=TH1F("","",nbin,binlow,binhigh)
            histo.Sumw2(kTRUE)
            histoSystUnc=TH1F("","",nbin,binlow,binhigh)
            #
            for bundle in bundles:
                histo.Reset("MICES")
                histoSystUnc.Reset("MICES")
                # Loop over all contributions for a given local selection, variable, and bundle
                for prebundleName in [hitemvar for hitemvar in hitems if hitemvar.endswith(bundle+"_"+var) and hitemvar.startswith("h_"+str(ilocal)+"_localsel_") ]:
                    h_=inputR.Get(prebundleName)
                    histo.Add(h_)
                    # Add flat uncertainty
                    flatuncertainty=relflatuncsdic[prebundleName[:-len(var)]]
                    print "LocalSelection: ",localselection[ilocal]," ",prebundleName,"  key: ",prebundleName[:-len(var)],"  flat uncertainty: ",flatuncertainty
                    hUnc_=h_.Clone("hUnc_")
                    hUnc_.Scale(float(flatuncertainty))
                    histoSystUnc.Add(hUnc_)
                # ----
                print "Added bundle(",bundle,") histo: ",histo.Integral(0,histo.GetNbinsX()+1)
                histointegral=0.0
                for ibin in xrange(1,histo.GetNbinsX()+2) : histointegral=histointegral+float(max(histo.GetBinContent(ibin),0))
                #
                if   bundle is "Data"              : histoDataArrayPerLocalSel.append((histo.Clone("histo_"+var+bundle),bundle,var,histointegral))
                elif bundle is "MisidentifiedSTAT" : histoMisidentifiedSTATArrayPerLocalSel.append((histo.Clone("histo_"+var+bundle),var+bundle))
                else                               : 
                    if bundle in pset.templateBundle and pset.templateVarContains in var : 
                        historef=histoMCRefArray[0][i][0]
                        if historef.Integral() > 0: historef.Scale(histo.Integral()/historef.Integral())
                        histointegral=max(historef.Integral(),0)
                        histoMCArrayPerLocalSel.append((historef.Clone("histo_"+var+bundle),histoSystUnc.Clone("histoSystUncertainty_"+var+bundle),bundle,var,histointegral))
                    elif bundle in pset.smoothBundle and pset.smoothVarContains in var : 
                        histosmooth=smooth(histo)#.Clone("histosmooth_"+var+bundle))
                        histosmooth.SetDirectory(0)
                        histointegral=max(histosmooth.Integral(),0)
                        histoMCArrayPerLocalSel.append((histosmooth.Clone("histo_"+var+bundle),histoSystUnc.Clone("histoSystUncertainty_"+var+bundle),bundle,var,histointegral))
                    else : histoMCArrayPerLocalSel.append((histo.Clone("histo_"+var+bundle),histoSystUnc.Clone("histoSystUncertainty_"+var+bundle),bundle,var,histointegral))
        #                    
        # Sort MC contributions in increasing order (needed for plotting purposes):
        histoMCArrayPerLocalSel = sorted(histoMCArrayPerLocalSel, key=getKey4)
        histoDataArray.append(histoDataArrayPerLocalSel)
        histoMCArray.append(histoMCArrayPerLocalSel)
        histoMisidentifiedSTATArray.append(histoMisidentifiedSTATArrayPerLocalSel)

    #print "\nChecking histoMCArray"
    #for k in histoMCArray:
    #    print k
    #print "\nChecking histoDataArray"
    #for k in histoDataArray:
    #    print k
    #print

    print "histoMisidentifiedSTATArray: ",histoMisidentifiedSTATArray

    # ----------------------------------------------------------------
    # Plotting
    # ----------------------------------------------------------------
    if usePublicationStyle : takeLegendOutside=False; showSelection=False; useCustomXlabels=True
    else                   : takeLegendOutside=True
    # Create canvas, legend:
    canvasXsize=650; canvasYsize=650
    if doRatio : canvasXsize=750; canvasYsize=750
    if takeLegendOutside : canvasXsize=1000
    can1 = plotSettings.CreateCanvas1D("can1",canvasXsize,canvasYsize); can1.cd()
    legYhigh = 0.9;  legYlow = legYhigh-(len(bundles)+int(plotUncUpperPanel)-len(overlayBundles)+1)*0.08
    legOverlayYhigh = legYlow; legOverlayYlow = legYlow-(len(overlayBundles))*0.08; 
    legXhigh = 0.95; legXlow = 0.71; 
    if usePublicationStyle :
        #legYhigh = 0.9;  legYlow = legYhigh-(len(bundles)+int(plotUncUpperPanel)-len(overlayBundles))*0.05; 
        legYhigh = 0.9;  legYlow = legYhigh-(len(bundles)+int(plotUncUpperPanel)-len(overlayBundles))*0.03; 
        legOverlayYhigh = legYlow; legOverlayYlow = legYlow-(len(overlayBundles))*0.0275; 
        #legXhigh = 0.92; legXlow = 0.6; 
        legXhigh = 0.40; legXlow = 0.90; 
        #print "No of bundles: ",(len(bundles))
        #print "Legend Size:: legYhigh-legYlow: ",legYhigh,"-",legYlow

    #leg = TLegend(0.6,legYlow,0.92,legYhigh); plotSettings.SetLegendStyle(leg)
    #leg = TLegend(0.75,0.5,0.95,0.9); plotSettings.SetLegendStyle(leg)
    leg = TLegend(legXlow,legYlow,legXhigh,legYhigh); plotSettings.SetLegendStyle(leg)
    legOverlay = TLegend(legXlow,legOverlayYlow,legXhigh,legOverlayYhigh); plotSettings.SetLegendStyle(legOverlay)
    if usePublicationStyle : leg.SetNColumns(2); legOverlay.SetNColumns(2)

    # Ratio plot customization:
    padFrac=0
    if doRatio : padFrac=0.26
    pad1 = plotSettings.CreatePad("pad1",padFrac,1); pad1.Draw()
    pad2 = plotSettings.CreatePad("pad2",0,padFrac*1.035); pad2.Draw(); pad2.SetLogy(0)
    if doRatio==False : pad1.SetTopMargin(0.07); pad1.SetBottomMargin(0.13)
    if doRatio : pad1.SetTopMargin(0.07); pad1.SetBottomMargin(0.0135); pad2.SetTopMargin(0.05); pad2.SetBottomMargin(0.4)
    #if takeLegendOutside : pad1.SetRightMargin(0.27); pad2.SetRightMargin(0.27);
    if takeLegendOutside : pad1.SetRightMargin(0.3065); pad2.SetRightMargin(0.3065);
    else                 : pad1.SetRightMargin(0.10); pad2.SetRightMargin(0.10);

    for ilocal in range(len(localselection)):
        for ivar,(var,nbin,binlow,binhigh,xlabel,isDiscrete) in enumerate(variables):
            print "\nBEGIN::Plotting variable: ",var,"  LocalSelection: ",localselection[ilocal]
            hMC           = THStack("hMC_"+str(ivar),"stack1_"+str(ivar))
            hOverlay      = THStack("hOverlay_"+str(ivar),"stack1_"+str(ivar))
            hMCsum        = TH1F("hMCsum_"+str(ivar),"",nbin,binlow,binhigh) #with Stat and Syst (if given) Uncertainties
            hMCsumSysUnc  = TH1F("hMCsumSysUnc_"+str(ivar),"",nbin,binlow,binhigh)
            hData         = TH1F("hData_"+str(ivar),"",nbin,binlow,binhigh)
            hFrame        = TH1F("hFrame_"+str(ivar),"",nbin,binlow,binhigh)
            hFrame2       = TH1F("hFrame2_"+str(ivar),"",nbin,binlow,binhigh)
            hRatioPlotErrorOnMC         = TH1F("hRatioPlotErrorOnMC_"+str(ivar),"",nbin,binlow,binhigh)
            hRatioPlotErrorOnMCStatOnly = TH1F("hRatioPlotErrorOnMCStatOnly_"+str(ivar),"",nbin,binlow,binhigh)
            hRatioPlotErrorOnData       = TGraphAsymmErrors()
            #hRatioPlotErrorOnData  = TH1F("hRatioPlotErrorOnData_"+str(ivar),"",nbin,binlow,binhigh)
            hZScore       = TH1F("hZScore_"+str(ivar),"",nbin,binlow,binhigh)
            h0SigmaLine   = TH1F("h0SigmaLine_"+str(ivar),"",nbin,binlow,binhigh)
            h1SigmaBand   = TH1F("h1SigmaBand_"+str(ivar),"",nbin,binlow,binhigh)
            h2SigmaBand   = TH1F("h2SigmaBand_"+str(ivar),"",nbin,binlow,binhigh)
            leg.Clear()
            legOverlay.Clear()
            hMCsum.Sumw2(kTRUE)
            #
            # extract max for overlaid histograms for y-axis purposes:
            overlayMax = 0.
            # -----
            for j,(h,bundle,var2,integral) in enumerate(histoDataArray[ilocal]):
                if var!=var2: continue
                print "Contribution: ",bundle," ",var," : ",h.GetEntries()
                """
                oflow=h.GetBinContent(h.GetNbinsX()+1)
                oflowE=h.GetBinError(h.GetNbinsX()+1)
                lastb=h.GetBinContent(h.GetNbinsX())
                lastbE=h.GetBinError(h.GetNbinsX())
                h.SetBinContent(h.GetNbinsX(),oflow+lastb)
                h.SetBinError(h.GetNbinsX(),pow(pow(oflowE,2)+pow(lastbE,2),0.5))
                """
                hData.Add(h)
                hData.SetMarkerStyle(9)
                hData.SetLineColor(1)
                hData.SetMarkerColor(1)
                hData.SetFillColor(1)
                # ---
                if useBundleColors :
                    hData.SetLineColor(int(bundlecolorsdic[bundle]))
                    hData.SetMarkerColor(int(bundlecolorsdic[bundle]))
                    hData.SetFillColor(int(bundlecolorsdic[bundle]))
                # ---
                if  usePublicationStyle : leg.AddEntry(hData,bundle,"PE")
                else                    : leg.AddEntry(hData,bundle+" ["+str(int(integral))+"]","PE")
                #else                     : leg.AddEntry(hData,bundle+" ["+str('%.2E' % integral)+"]","PE")
                # ---
                if not os.path.exists(rootFileName+"/LocalSelection"+str(ilocal)+"/"): os.makedirs(rootFileName+"/LocalSelection"+str(ilocal)+"/")
                hData.SetNameTitle("h_"+str(variables[ivar][0])+"_Data",globalselection+"&&"+localselection[ilocal])
                hData.SaveAs(rootFileName+"/LocalSelection"+str(ilocal)+"/LS"+str(ilocal)+"_"+str(variables[ivar][0])+"_Data.root")
            # -----
            icolor=2
            for j,(h,hSysUnc,bundle,var2,integral) in enumerate(histoMCArray[ilocal]):
                if var!=var2: continue
                print "Contribution: ",bundle," ",var," : ",h.GetEntries()
                """
                oflow=h.GetBinContent(h.GetNbinsX()+1)
                oflowE=h.GetBinError(h.GetNbinsX()+1)
                lastb=h.GetBinContent(h.GetNbinsX())
                lastbE=h.GetBinError(h.GetNbinsX())
                h.SetBinContent(h.GetNbinsX(),oflow+lastb) # Manually add overflows
                h.SetBinError(h.GetNbinsX(),pow(pow(oflowE,2)+pow(lastbE,2),0.5)) # Manually add overflow errors (statistical)
                """
                # truncate negative bins
                for ibin in xrange(1,h.GetNbinsX()+2): #include overflow bin
                    if h.GetBinContent(ibin)<0 : 
                        print bundle," TRUNCATED BIN(",ibin,"): ",h.GetBinContent(ibin)," --> 0"
                        h.SetBinContent(ibin,0.0)
                # ---
                # set stat errors for special case of Misidentified background
                if bundle is "Misidentified" and len(histoMisidentifiedSTATArray)>0 :
                    for iq,(staterrhisto,staterrhistoname) in enumerate(histoMisidentifiedSTATArray[ilocal]) : 
                        #if staterrhistoname == var+bundle+"STAT" : 
                        if staterrhistoname is var+bundle+"STAT" : 
                            print "Set smart statistical errors for: ",bundle
                            for ibin in xrange(1,staterrhisto.GetNbinsX()+1) :
                                if staterrhisto.GetBinContent(ibin)>0. : h.SetBinError( ibin, (1./pow(staterrhisto.GetBinContent(ibin),0.5))*h.GetBinContent(ibin) )
                # ---
                plotSettings.SetHistoStyle(h,hData.GetBinWidth(0),xlabel)
                h.SetFillStyle(1001)
                h.SetFillColor(icolor)
                h.SetLineColor(icolor)
                h.SetMarkerColor(icolor)
                h.SetMarkerStyle(9)
                # ---
                if useBundleColors :
                    h.SetFillColor(int(bundlecolorsdic[bundle]))
                    h.SetLineColor(int(bundlecolorsdic[bundle]))
                    h.SetMarkerColor(int(bundlecolorsdic[bundle]))
                # ---
                if not os.path.exists(rootFileName+"/LocalSelection"+str(ilocal)+"/"): os.makedirs(rootFileName+"/LocalSelection"+str(ilocal)+"/")
                h.SetNameTitle("h_"+str(variables[ivar][0])+"_"+bundle,globalselection+"&&"+localselection[ilocal])
                hSysUnc.SetNameTitle("h_"+str(variables[ivar][0])+"_"+bundle+"_SysUnc",globalselection+"&&"+localselection[ilocal])
                h.SaveAs(rootFileName+"/LocalSelection"+str(ilocal)+"/LS"+str(ilocal)+"_"+str(variables[ivar][0])+"_"+bundle+".root")
                hSysUnc.SaveAs(rootFileName+"/LocalSelection"+str(ilocal)+"/LS"+str(ilocal)+"_"+str(variables[ivar][0])+"_"+bundle+"_SysUnc.root")
                # ---
                if bundle in overlayBundles :
                    #if "380" in bundle : h.SetLineStyle(2) 
                    hOverlay.Add(h)
                    overlayMax = max( overlayMax, h.GetBinContent(h.GetMaximumBin()) )
                # ---
                if bundle not in overlayBundles : 
                    hMC.Add(h)
                    hMCsum.Add(h)
                    hMCsumSysUnc.Add(hSysUnc)
                # ---
                icolor+=1
            # -----
            # Add flat systematic uncertainties to hMCsum, store stat errors separately:
            hMCsumWithStatUncOnly=hMCsum.Clone(); hMCsumWithStatUncOnly.Sumw2(kTRUE); hMCsumWithStatUncOnly.Reset("MICES")
            for ibin in xrange(1,hMCsum.GetNbinsX()+1): # overflows have already been taken care of above!
                hMCsumWithStatUncOnly.SetBinContent(ibin,hMCsum.GetBinContent(ibin)); hMCsumWithStatUncOnly.SetBinError(ibin,hMCsum.GetBinError(ibin))
                hMCsum.SetBinError(ibin,pow(pow(hMCsum.GetBinError(ibin),2)+pow(hMCsumSysUnc.GetBinContent(ibin),2),0.5)) # hMCsum includes stat and syst uncertainties
            # -----
            icolor=2
            for j,(h,hSysUnc,bundle,var2,integral) in enumerate(reversed(histoMCArray[ilocal])):
                if var!=var2: continue
                h.SetFillStyle(1001)
                h.SetFillColor(icolor)
                h.SetLineColor(icolor)
                h.SetMarkerColor(icolor)
                # ---
                if useBundleColors :
                    h.SetFillColor(int(bundlecolorsdic[bundle]))
                    h.SetLineColor(int(bundlecolorsdic[bundle]))
                    h.SetMarkerColor(int(bundlecolorsdic[bundle]))
                # ---
                if bundle in overlayBundles : 
                    h.SetLineWidth(2)
                    h.SetFillStyle(0)
                    h.SetFillColor(0)
                # ---
                if  usePublicationStyle : 
                    if bundle not in overlayBundles : leg.AddEntry(h,bundle,"F")
                    if bundle     in overlayBundles : legOverlay.AddEntry(h,bundle,"F")
                else                    :
                    if bundle not in overlayBundles :
                        if integral<0.01 : leg.AddEntry(h,bundle+" ["+str("<0.01")+"]","F")
                        else             : leg.AddEntry(h,bundle+" ["+str(round(integral,2))+"]","F")
                    if bundle     in overlayBundles :
                        if integral<0.01 : legOverlay.AddEntry(h,bundle+" ["+str("<0.01")+"]","F")
                        else             : legOverlay.AddEntry(h,bundle+" ["+str(round(integral,2))+"]","F")
                # ---
                icolor+=1
            # -----
            # Define a new "numerator" for obs/expected ratios! (For OPTIONAL USE, for example for closure tests in MC)
            if    useOverlayForRatio : hNumerator = (hOverlay.GetStack().Last()).Clone() # This assumes there is only 1 overlay histogram!
            else                     : hNumerator = hData.Clone()            
            if usePoissonErrorBarsOnData :
                hNumerator.Sumw2(kFALSE)
                hNumerator.SetBinErrorOption(TH1.kPoisson)
            # -----
            if not usePublicationStyle :
                if hMCsum.Integral(0,hMCsum.GetNbinsX()+1) > 0 : 
                    leg.SetHeader(str(ratioLabel)+": "+str(format(hNumerator.Integral(0,hNumerator.GetNbinsX()+1)/hMCsum.Integral(0,hMCsum.GetNbinsX()+1),'.2f')))
                else :
                    leg.SetHeader(str(ratioLabel)+": NA")
            # Plot main canvas:
            pad1.cd()
            plotSettings.SetHistoStyle(hFrame,hData.GetBinWidth(0),xlabel)
            if usePublicationStyle : yrangescale = max( 20, pow( 10. , len(bundles)/2. ) )  #120.*(pow((max(1,len(bundles)))-1,2))
            else                   : yrangescale = 20.
            histoymax = max(hData.GetBinContent(hData.GetMaximumBin()),hMCsum.GetBinContent(hMCsum.GetMaximumBin()))
            histoymax = max(histoymax, overlayMax)
            hFrame.GetYaxis().SetRangeUser(0.05,histoymax*yrangescale)
            #hFrame.GetYaxis().SetRangeUser(0.0002,histoymax*yrangescale)
            #hFrame.GetYaxis().SetRangeUser(0.05,130000) # L3 SR
            #hFrame.GetYaxis().SetRangeUser(0.05,2000) # L4 SR
            hFrame.Draw("HIST")
            if showSelection : 
                #if localselection[ilocal] == "1" : hFrame.SetTitle(globalselection)
                if localselection[ilocal] is "1" : hFrame.SetTitle(globalselection)
                else :  hFrame.SetTitle(globalselection+"&&"+localselection[ilocal])
            else : hFrame.SetTitle("")
            if useCustomXlabels : hFrame.GetXaxis().SetTitle(xlabel)
            else : hFrame.GetXaxis().SetTitle(var)
            if isDiscrete and nbin<21 : hFrame.GetXaxis().SetNdivisions(200+nbin)
            hMC.Draw("HISTsame")
            # -----
            if plotUncUpperPanel :
                hMCsum.SetFillStyle(3154)
                hMCsum.SetMarkerStyle(9)
                hMCsum.SetMarkerSize(0)
                hMCsum.SetFillColor(kGray+2)
                hMCsum.SetLineColor(kGray)
                hMCsum.Draw("E2same")
            # -----
            hOverlay.Draw("HISTNOSTACKsame")
            # -----
            # Do "poisson" error bars on data:
            if usePoissonErrorBarsOnData :
                hData.Sumw2(kFALSE);
                hData.SetBinErrorOption(TH1.kPoisson);
                hData.Draw("PE0same")
            else : hData.Draw("PEsame")
            # -----
            if plotUncUpperPanel : leg.AddEntry(hMCsum,"Uncertainty","F")
            leg.Draw()
            legOverlay.Draw()
            gPad.RedrawAxis()
            # -----
            # Print CMS and lumi labels:
            if usePublicationStyle:
                cmslabel      = TLatex(); cmslabel.SetTextFont(61);      cmslabel.SetTextAlign(11);      cmslabel.SetNDC();      cmslabel.SetTextSize(0.052)
                cmsextralabel = TLatex(); cmsextralabel.SetTextFont(52); cmsextralabel.SetTextAlign(11); cmsextralabel.SetNDC(); cmsextralabel.SetTextSize(0.052)
                lumilabel     = TLatex(); lumilabel.SetTextFont(42);     lumilabel.SetTextAlign(31);     lumilabel.SetNDC();     lumilabel.SetTextSize(0.052)
                #cmslabel.DrawLatex(0.13,0.945,"CMS")
                cmslabel.DrawLatex(0.128,0.945,"CMS")
                cmsextralabel.DrawLatex(0.22,0.945,"Preliminary")
                lumistr = '%s' % float('%.3g' % (lumi/1000.))
                #lumilabel.DrawLatex(0.882,0.945,lumistr+" fb^{-1} (13 TeV)")
                if    pset.year : lumilabel.DrawLatex(0.9015,0.945,lumistr+" fb^{-1} ("+str(pset.year)+", 13 TeV)")
                #if    pset.year : lumilabel.DrawLatex(0.9015,0.945,"("+str(pset.year)+", 13 TeV)")
                else            : lumilabel.DrawLatex(0.9015,0.945,lumistr+" fb^{-1} (13 TeV)")
            # -----
            # -----------------------------
            # Plot Ratio canvas:
            if doRatio :  
                pad2.cd()
                #if = pset.CustomRatio = "" :
                #hNumerator  = hData.Clone()
                #hNumerator  = (hOverlay.GetStack().Last()).Clone()
                #                hDenominator            = hMCsum.Clone()
                #                hDenominatorStatUncOnly = hMCsumWithStatUncOnly.Clone()
                # -------
                if doZscore: # Local z-score calculation:
                    print "zScore Calculation.."
                    for ibin in xrange(1,hMCsum.GetNbinsX()+1):
                        # zScore(Obs,Exp,ExpError,debug=False)
                        if hMCsum.GetBinContent(ibin)>0: zscore=Stats.zScore(hNumerator.GetBinContent(ibin),hMCsum.GetBinContent(ibin),hMCsum.GetBinError(ibin)/hMCsum.GetBinContent(ibin), debug=True)
                        else : zscore=-99
                        hZScore.SetBinContent(ibin,zscore)
                        h2SigmaBand.SetBinContent(ibin,0)
                        h2SigmaBand.SetBinError(ibin,2)
                        h1SigmaBand.SetBinContent(ibin,0)
                        h1SigmaBand.SetBinError(ibin,1)
                    #
                    plotSettings.SetRatioHistoStyle(hFrame2,pset)
                    hFrame2.GetYaxis().SetTitle(str(ratioLabel))
                    if isDiscrete and nbin<21 : hFrame2.GetXaxis().SetNdivisions(200+nbin)
                    hFrame2.GetYaxis().SetTitle("Z-score")
                    hFrame2.GetYaxis().SetRangeUser(-3,3)
                    hFrame2.GetYaxis().SetLabelSize(0.12)
                    hFrame2.GetXaxis().SetLabelSize(0.12)
                    hFrame2.GetYaxis().SetLabelOffset(0.0133)
                    hFrame2.Draw("HIST")
                    if useCustomXlabels : hFrame2.GetXaxis().SetTitle(xlabel)
                    else : hFrame2.GetXaxis().SetTitle(var)
                    #
                    h2SigmaBand.SetFillColor(18)
                    h2SigmaBand.SetLineColor(18)
                    h2SigmaBand.SetMarkerColor(18)
                    h2SigmaBand.SetFillStyle(1001)
                    h2SigmaBand.Draw("E2same")
                    #
                    h1SigmaBand.SetFillColor(16)
                    h1SigmaBand.SetLineColor(16)
                    h1SigmaBand.SetMarkerColor(16)
                    h1SigmaBand.SetFillStyle(1001)
                    h1SigmaBand.Draw("E2same")
                    #
                    h0SigmaLine.SetLineColor(1)
                    h0SigmaLine.Draw("HISTsame")
                    #
                    hZScore.SetMarkerStyle(9)
                    hZScore.SetLineColor(1)
                    hZScore.Draw("Psame")
                    #
                    gPad.RedrawAxis()
                # -------
                else : # observed/expected ratio calculation:
                    hRatioPlotErrorOnData_MultiArray= [[0 for x in range(hMCsum.GetNbinsX()+1)] for y in range(5)]
                    for ibin in xrange(1,hMCsum.GetNbinsX()+1):
                        bincenter=hMCsum.GetBinCenter(ibin)
                        ratio=-99
                        upErr=0
                        downErr=0
                        if hMCsum.GetBinContent(ibin)>0 :
                            ratio=hNumerator.GetBinContent(ibin)/hMCsum.GetBinContent(ibin)
                            upErr=hNumerator.GetBinErrorUp(ibin)/hMCsum.GetBinContent(ibin)
                            downErr=hNumerator.GetBinErrorLow(ibin)/hMCsum.GetBinContent(ibin)
                        hRatioPlotErrorOnData_MultiArray[0][ibin-1]=bincenter
                        hRatioPlotErrorOnData_MultiArray[1][ibin-1]=ratio
                        hRatioPlotErrorOnData_MultiArray[2][ibin-1]=upErr
                        hRatioPlotErrorOnData_MultiArray[3][ibin-1]=downErr
                        hRatioPlotErrorOnData_MultiArray[4][ibin-1]=hMCsum.GetBinWidth(ibin)*0.5
                    # --
                    hRatioPlotErrorOnData = TGraphAsymmErrors(hMCsum.GetNbinsX(),
                                                              array('d',hRatioPlotErrorOnData_MultiArray[0]),
                                                              array('d',hRatioPlotErrorOnData_MultiArray[1]),
                                                              array('d',hRatioPlotErrorOnData_MultiArray[4]),
                                                              array('d',hRatioPlotErrorOnData_MultiArray[4]),
                                                              array('d',hRatioPlotErrorOnData_MultiArray[3]),
                                                              array('d',hRatioPlotErrorOnData_MultiArray[2]))
                    # --
                    for ibin in xrange(1,hMCsum.GetNbinsX()+1):
                        ratioError=99
                        ratioErrorStatOnly=99
                        if hMCsum.GetBinContent(ibin)>0                : ratioError         = hMCsum.GetBinError(ibin)/hMCsum.GetBinContent(ibin)
                        if hMCsumWithStatUncOnly.GetBinContent(ibin)>0 : ratioErrorStatOnly = hMCsumWithStatUncOnly.GetBinError(ibin)/hMCsumWithStatUncOnly.GetBinContent(ibin)
                        hRatioPlotErrorOnMCStatOnly.SetBinContent(ibin,1)
                        hRatioPlotErrorOnMCStatOnly.SetBinError(ibin,ratioErrorStatOnly)
                        hRatioPlotErrorOnMC.SetBinContent(ibin,1)
                        hRatioPlotErrorOnMC.SetBinError(ibin,ratioError)
                    #
                    plotSettings.SetRatioHistoStyle(hFrame2,pset)
                    if isDiscrete and nbin<21 : hFrame2.GetXaxis().SetNdivisions(200+nbin)
                    if not usePublicationStyle : hFrame2.GetYaxis().SetRangeUser(0.5,1.5) # Ratio pad y-axis range customization
                    if     usePublicationStyle : hFrame2.GetYaxis().SetRangeUser(0.0,2.0) # Ratio pad y-axis range customization
                    hFrame2.GetYaxis().SetNdivisions(405,True) 
                    hFrame2.GetYaxis().SetLabelSize(0.12)
                    hFrame2.GetXaxis().SetLabelSize(0.12)
                    hFrame2.GetYaxis().SetLabelOffset(0.014)
                    hFrame2.Draw("HIST")
                    if useCustomXlabels : hFrame2.GetXaxis().SetTitle(xlabel)
                    else : hFrame2.GetXaxis().SetTitle(var)
                    #
                    if plotStatSystUnc :
                        hRatioPlotErrorOnMC.SetFillColor(17)
                        hRatioPlotErrorOnMC.SetLineColor(17)
                        hRatioPlotErrorOnMC.SetMarkerColor(17)
                        hRatioPlotErrorOnMC.SetFillStyle(1001)
                        hRatioPlotErrorOnMC.Draw("E2same")
                        hRatioPlotErrorOnMCStatOnly.SetFillColor(15)
                        hRatioPlotErrorOnMCStatOnly.SetLineColor(15)
                        hRatioPlotErrorOnMCStatOnly.SetMarkerColor(15)
                        hRatioPlotErrorOnMCStatOnly.SetFillStyle(1001)
                        hRatioPlotErrorOnMCStatOnly.Draw("E2same")
                    else :
                        hRatioPlotErrorOnMCStatOnly.SetFillColor(kGray)
                        hRatioPlotErrorOnMCStatOnly.SetLineColor(kGray)
                        hRatioPlotErrorOnMCStatOnly.SetMarkerColor(kGray)
                        hRatioPlotErrorOnMCStatOnly.SetFillStyle(1001)
                        hRatioPlotErrorOnMCStatOnly.Draw("E2same")
                    #
                    hRatioPlotErrorOnData.SetLineStyle(1)
                    hRatioPlotErrorOnData.SetMarkerStyle(9)
                    hRatioPlotErrorOnData.SetLineColor(1)
                    hRatioPlotErrorOnData.Draw("PEZ0same")
                    #
                    gPad.RedrawAxis()
                    #
                    # debugging
                    # print "\nRatio Values: ",var,"  LocalSelection: ",localselection[ilocal]
                    # for ibin in xrange(1,hNumerator.GetNbinsX()+1):
                        # print "BinCenter: ",hNumerator.GetBinCenter(ibin),
                        # print "  Ratio: ",hRatioPlotErrorOnData.GetY()[ibin-1],
                        # print "  Obs: ",hNumerator.GetBinContent(ibin),"  +",hNumerator.GetBinErrorUp(ibin)," -",hNumerator.GetBinErrorLow(ibin),
                        # print "  Exp: ",hMCsum.GetBinContent(ibin)," +/- ",hMCsum.GetBinError(ibin)
                    # print 

                # -----------------------------
            ####
            ####
            # Save each page as canvas
            if not os.path.exists(rootFileName+"/LocalSelection"+str(ilocal)+"/"): os.makedirs(rootFileName+"/LocalSelection"+str(ilocal)+"/")
            can1.SaveAs(rootFileName+"/LocalSelection"+str(ilocal)+"/LS"+str(ilocal)+"_"+str(variables[ivar][0])+"_Canvas.root")
            if len(variables)==1 and len(localselection)==1 : can1.SaveAs(rootFileName+"/"+rootFileName+".pdf"); 
            else :
                if ivar==0 and ilocal==0 : can1.SaveAs(rootFileName+"/"+rootFileName+".pdf(")
                elif ivar==len(variables)-1 and ilocal==len(localselection)-1 : can1.SaveAs(rootFileName+"/"+rootFileName+".pdf)")
                else : can1.SaveAs(rootFileName+"/"+rootFileName+".pdf")
                print "END::Plotting variable: ",var,"\n"
    print
    print "\nEND:: plotMaker\n"
    #
    printSelections(pset,rootFileName)
    return

# ---------------------------------------------------------------------------------

def printSelections(pset,rootFileName) :
    import os
    print "--------------------------------------------"
    print "*** PDF INFO ***"
    print "- -- -- -- -- -- -"
    print "File: ",str(os.getcwd())+"/"+rootFileName+"/"+rootFileName+".pdf"
    print "- -- -- -- -- -- -"
    print "Global Selection: ",pset.globalselection
    print "- -- -- -- -- -- -"
    for ilocalsel,localsel in enumerate(pset.localselection) :
        print "Local Selection: ",ilocalsel,
        print " pages: ",str(ilocalsel*len(pset.variables)+1),"-",str(ilocalsel*len(pset.variables)+len(pset.variables)+1),
        print " cut: ",localsel
    print "--------------------------------------------\n"
    return
    
# ---------------------------------------------------------------------------------


